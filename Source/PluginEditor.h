 /*
 ==============================================================================
 
 This file was auto-generated!
 
 It contains the basic framework code for a JUCE plugin editor.
 
 ==============================================================================
 */

#pragma once
#include <numeric>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include <cmath>
#include <map>

#define NegativeInfinity -66.f
#define MaxDecibels 12.f
#define TimerFreqHz 60



struct Tick {float y; int dB; };
//=================================================================

struct ValueHolderBase : Timer
{
    using Func = std::function<void()>;
    
    ValueHolderBase(float cv, int ht, Func f) :
    holdTime(ht),
    currentValue(cv),
    func(std::move(f))
    {
        peakTime = Time::currentTimeMillis();
        startTimerHz(TimerFreqHz);
    }
    
    virtual ~ValueHolderBase()
    {
        stopTimer();
    }
    
    void setHoldTime(int ms) { holdTime = ms; }
    
    float getCurrentValue() const { return currentValue; }
    
    virtual void updateHeldValue (float input) = 0;
    
    void timerCallback() override
    {
        if( func )
        {
            auto now = Time::currentTimeMillis();
            auto elapsedTime = now - peakTime;
            if( elapsedTime > holdTime )
                func();
        }
    }
protected:
    int holdTime = 0;
    float currentValue;
    int64 peakTime = 0;
    Func func;
};

struct ValueHolder : ValueHolderBase
{
    ValueHolder() : ValueHolderBase(0, 1000, [this]() { this->resetCurrentValue(); })
    {
        setThreshold(0);
        startTimerHz(TimerFreqHz);
    }
    bool isOverThreshold() const { return currentValue > threshold; }
    void setThreshold (float thresholdToSet) { threshold = thresholdToSet; }
    
    void updateHeldValue (float input) override
    {
        if (input > threshold )
        {
            peakTime = Time::currentTimeMillis();
            
            if( input > currentValue)
                currentValue = input;
        }
    }
private:
    void resetCurrentValue() { currentValue = threshold; }
    float threshold = 0.f;
};
//=================================================================
struct DecayingValueHolder : ValueHolderBase
{
    DecayingValueHolder() :
    ValueHolderBase(NegativeInfinity,
                    1000,
                    [this]()
                    {
                        this->currentValue -= this->decayRate;
                    })
    
    {
        decayRate = 0;
//        setDecayRate(3.f); // will be set by combo box
    }
    void setDecayRate(float dbPerSec)
    {
        decayRate = dbPerSec / TimerFreqHz;
    }
    void updateHeldValue (float input) override
    {
        if( input >= currentValue)
        {
            peakTime = Time::currentTimeMillis();
            currentValue = input;
        }
    }
    
    
    //    double easeOutSine( double t )
    //    {
    //        return 1 + std::sin( 1.5707963 * (--t) );
    //    }  // thought I moved this code
    
private:
    float decayRate;
};

//==============================================================================
template<typename T>
struct Averager
{
    Averager(size_t numElements, T initialValue)
    {
        resize(numElements, initialValue);
    }
    
    void clear(T initialValue)
    {
        resize(getSize(), initialValue);
    }
    
    void resize(size_t s, T initialValue)
    {
        container.clear();
        container.resize(s, initialValue);
        writeIndex = 0;
        //recompute your average
        sumElements();
        average();
    }
    
    void add(T t)
    {
        sum -= container[writeIndex]; // optimization
        sum += t;
        
        //store the new value in the container
        container[writeIndex] = t;
        
        //make sure the write index doesn't write past the end of the container
        ++writeIndex;
        if (writeIndex > getSize() - 1 )
        {
            writeIndex = 0;
        }
        
        //update the average
        average();
    }
    float getAverage() const
    {
        return currentAverage.load(); // read about atomic lib
    }
    size_t getSize() const
    {
        return container.size();
    }
    
private:
    void average()
    {
        currentAverage = static_cast<float>(sum) / static_cast<float>(getSize() );
    }
    void sumElements()
    {
        sum = 0;
        for(const auto& value : container)
        {
            sum += value;
        }
    }
    T sum{0};
    std::vector<T> container;
    std::atomic<float> currentAverage { };
    std::atomic<size_t> writeIndex{0};
};
//=================================================================
template<typename T>
struct CircularBuffer
{
    using DataType = std::vector<T>; //some container holding T's
    CircularBuffer(std::size_t s, T fillValue )
    {
        resize(s, fillValue);
    }
    void fill(T fillValue)
    {
        container.assign(getSize(), fillValue);
    }
    void fillAndResetIndex(T fillValue)
    {
        //don't resize - > fill instaed
        fill(fillValue);
        resetIndex();
    }
    void resize(std::size_t s, T fillValue)
    {
        container.clear();
        container.resize(s, fillValue);
        resetIndex();
    }
    void resetIndex()
    {
        writeIndex = 0;
    }
    void write(T t)
    {
        auto i = writeIndex.load();
        container[i] = t;
        //        DBG("value of " << t << " at: " << i);
        ++i; // i now points to the oldest index of the containter
        if(i > getSize() - 1)
            i = 0;
        writeIndex.store(i); // now write index points to the oldest index of the containter
    }
    
    DataType& getData()
    {
        return container;
    }
    
    size_t getReadIndex() const
    {
        return writeIndex;

    }
    size_t getSize() const
    {
        return container.size();
    }
private:
    DataType container;
    std::atomic<size_t> writeIndex { 0 };
};

//=================================================================

struct TextMeter : Component
{
    void paint(Graphics& g) override;
    void update(float decibels);
    void setHoldTime(int newHoldTime)
    {
        vh.setHoldTime(newHoldTime);
    }
    ValueHolder vh;
private:
    float inputDb;
};

//==============================================================================
struct Meter : Component //draws rectangle for the meter
{
    Meter();
    void paint(Graphics& g) override;
    void resized() override;
    void update(float decibels);
    void setDecayRate(float newRate)
    {
        dVH.setDecayRate(newRate);
    }
    void setHoldTime(int newHoldTime)
    {
        dVH.setHoldTime(newHoldTime);
    }
    std::vector<Tick> tickPos;
    bool showHoldTicks;
    bool useInfiniteHoldTime;
    void setInfiniteHold(bool ih)
    {
        useInfiniteHoldTime = ih;
    }
    void resetDecayingTick()
    {
        decayingTickValue = NegativeInfinity;
    }
private:
    DecayingValueHolder dVH;
    ColourGradient myGrad;
    float levelDb = NegativeInfinity;
    float decayingTickValue = NegativeInfinity;
};


//==============================================================================
struct Scale : Component
{
    Scale(bool scaleCentered);
    void paint(Graphics& g) override;
    void resized() override;
    void update(float value);
    bool centeredText;
    std::vector<Tick> tickPos;
    int meterOffset = 0;
};

//============================================================================
struct Histogram : Component
{
    Histogram(juce::String meterType);
    void paint(Graphics& g) override;
    void resized() override;
    void update(float decibels);
    void clear()
    {
        histoBuffer.fillAndResetIndex(NegativeInfinity);
    }
    void mouseDown (const MouseEvent &event) override
    {
        clear();
    }
private:
    juce::String _meterType;
    Path path;
    ColourGradient myGrad;
    CircularBuffer<float> histoBuffer{0, NegativeInfinity};
    std::vector<Tick> tickPos;
    Scale scale;
    Label histoLabel;
};

//==============================================================================
enum HistoViewType
{
    Stacked,
    SideBySide
};

struct HistogramContainer : Component
{
    HistogramContainer();
    void resized() override;
    void setHistoView(HistoViewType vt)
    {
        histoView = vt;
        resized();
    }
    Histogram rmsHisto, peakHisto;
private:
    HistoViewType histoView;
};

//==============================================================================
struct Goniometer : Component
{
    Goniometer(juce::AudioBuffer<float> & _myBuffer);
    void paint (Graphics& g) override;
    void update(); // do not need to pass
    void resized() override;
    void generateImage();
    void updateScaleSize(double newPercentage)
    {
        scaleSize = newPercentage;
    }
    juce::Point<float> getXY (float leftChan, float rightChan, Point<float> center, float radius);
private:
    double scaleSize;
    const float subtract3dB = Decibels::decibelsToGain(-3.f);
    Path path;
    Image gonioGrid;
    ColourGradient myGrad;
    juce::AudioBuffer<float> & myBuffer;
};
//======================================================================

struct CorrelationMeter : Component
{
    void paint(Graphics& g) override;
    void update(float correlation);
    void resized() override;
private:
    ColourGradient positiveGrad, negativeGrad;
    float correlationValue;
};

//======================================================================

using namespace dsp;
struct CorrelationComputation : Component
{
    using FilterType = IIR::Filter<float>;
    CorrelationComputation(juce::AudioBuffer<float> & _myBuffer, double sampleRate);
    void paint (Graphics& g) override;
    void update();
    void resized() override;    
    float computeCorrelation();
private:
    CorrelationMeter correlationMeter, avrgdCorrelationMeter;
    int numberOfBuffersToAverage = 7; // to be changed by user via comboBox
    int bufferSize = 256; // default value to be overriten by class constructor initializer
    Averager<float> averager{static_cast<size_t>(bufferSize * numberOfBuffersToAverage), 0};
    Averager<float> instantCorr{static_cast<size_t>(bufferSize), 0};
    std::array<FilterType, 3 > filters;
    juce::AudioBuffer<float> & myBuffer;
};


//==============================================================================
struct GonioAndCorrelationWidget : Component
{
    GonioAndCorrelationWidget(juce::AudioBuffer<float> & myBuffer, double sampleRate);
    void paint(Graphics& g) override;
    void resized() override;
    void update(const juce::AudioBuffer<float> &buffer);
    void updateScaleSize(double newPercentage)
    {
        goniometer.updateScaleSize(newPercentage);
    }
private:
    Goniometer goniometer;
    CorrelationComputation correlationMeter;
};
//==================================================================
enum MeterViewType
{
    Both,
    Avg,
    Peak
};

struct MacroMeter : Component
{
    MacroMeter(bool avOnLeft);
    void resized() override;
    void update(float decibels);
    bool averageOnLeft;
    Meter& getInstantMeterObject()
    {
        return instantMeter;
    }
    void setDecayRate(float newRate)
    {
        instantMeter.setDecayRate(newRate);
        averageMeter.setDecayRate(newRate);
    }
    void setHoldTime(int newHoldTime)
    {
        instantMeter.setHoldTime(newHoldTime);
        averageMeter.setHoldTime(newHoldTime);
        textMeter.setHoldTime(newHoldTime);
    }
    void setAveragerSize (int newSize)
    {
        int msPerSec = 1000/TimerFreqHz;
        avrgSize = static_cast<size_t>(newSize / msPerSec);
    }
    bool showHoldTicks(bool ticksShown)
    {
        instantMeter.showHoldTicks = ticksShown;
        averageMeter.showHoldTicks = ticksShown;
        return 0;
    }
    void setMeterView(MeterViewType vt)
    {
        meterView = vt;
        resized();
    }
    void setInfiniteHold(bool ih)
    {
        instantMeter.setInfiniteHold(ih);
        averageMeter.setInfiniteHold(ih);
    }
    void resetDecayingTick()
    {
        instantMeter.resetDecayingTick();
        averageMeter.resetDecayingTick();
    }
    
private:
    MeterViewType meterView;
    Meter instantMeter, averageMeter;
    size_t avrgSize = 3;
    Averager<float> averager{avrgSize, 0};
    TextMeter textMeter;
    Label meterLabel;
};

//==================================================================
struct StereoMeter : Component
{
    StereoMeter(juce::String meterType);
    void paint(Graphics& g) override;
    void resized() override;
    void update(float leftChannel, float rightChannel);
    void setDecayRate(float newRate)
    {
        leftChan.setDecayRate(newRate);
        rightChan.setDecayRate(newRate);
    }
    void setHoldTime(int newHoldTime)
    {
        leftChan.setHoldTime(newHoldTime);
        rightChan.setHoldTime(newHoldTime);
    }
    void setAveragerSize (int newSize)
    {
        leftChan.setAveragerSize(newSize);
        rightChan.setAveragerSize(newSize);
    }
    bool showHoldTicks(bool ticksShown)
    {
        leftChan.showHoldTicks(ticksShown);
        rightChan.showHoldTicks(ticksShown);
        return 0;
    }
    void setMeterView(MeterViewType vt)
    {
        leftChan.setMeterView(vt);
        rightChan.setMeterView(vt);
    }
    void setInfiniteHold(bool ih)
    {
        leftChan.setInfiniteHold(ih);
        rightChan.setInfiniteHold(ih);
    }
    void resetDecayingTick()
    {
        leftChan.resetDecayingTick();
        rightChan.resetDecayingTick();
    }
private:
    juce::String _meterType;
    MacroMeter leftChan, rightChan;
    Label meterLabel;
    Scale scale;
};
//==================================================================


class BoxLookAndFeel : public LookAndFeel_V4
{
public:
    Font getComboBoxFont (ComboBox& /*box*/) override
    {
        return getMenuFont();
    }
    
    Font getPopupMenuFont() override
    {
        return getMenuFont();
    }
    
    void drawComboBox ( Graphics & g,
                       int     width,
                       int     height,
                       bool     isButtonDown,
                       int     buttonX,
                       int     buttonY,
                       int     buttonW,
                       int     buttonH,
                       ComboBox & cb) override
    {
        g.fillAll(Colours::black);
        g.setColour(Colours::gold.darker() );
        g.drawRect(cb.getLocalBounds());
        auto line = Line<float>(55.f, 12.f, 55.f, 20.f);
        g.drawArrow(line, 7.f, 14.f, 10.f);
    }
    
    void drawPopupMenuItem    (    Graphics & g,
                          const Rectangle< int > &     area,
                          bool     isSeparator,
                          bool     isActive,
                          bool     isHighlighted,
                          bool     isTicked,
                          bool     hasSubMenu,
                          const String &     text,
                          const String &     shortcutKeyText,
                          const Drawable *     icon,
                          const Colour *     textColour
                               ) override ;
    
    void getIdealPopupMenuItemSize    (const String&     text,
                                  bool     isSeparator,
                                  int     standardMenuItemHeight,
                                  int &     idealWidth,
                                  int &     idealHeight
                                  ) override;

    
private:
    Font getMenuFont()
    {
        return Font (Font::getDefaultMonospacedFontName(), "Narrow", 12.f);
    }
};
//==================================================================
enum BoxIndex
{
    DecayRate,
    AverageAmount,
    MeterView,
    HoldTime,
    HistoView,
    ScaleSlider
};

//==================================================================
struct SelectBoxes : Component
{
    SelectBoxes();
    void resized() override;
    void paint(Graphics& g) override;
    juce::Slider scaleSlider{ Slider::Rotary, Slider::NoTextBox };
    juce::TextButton holdButton{"HOLD"};
    juce::TextButton resetHoldButton{"RESET TICK"};
private:
    std::array<StringArray, 5> dropDownLists;
    std::array<Label, 6> labels;
    juce::Font textFont { 10.0f };
    BoxLookAndFeel boxLandF;
public:
    std::array<ComboBox, 5> boxes;
};

//==============================================================================
class Pfmcpp_project10AudioProcessorEditor  : public AudioProcessorEditor, public Timer
{
public:
    using DataType = AudioBuffer<float>;
    Pfmcpp_project10AudioProcessorEditor (Pfmcpp_project10AudioProcessor&);
    ~Pfmcpp_project10AudioProcessorEditor();
    
    void paint (Graphics&) override;
    void resized() override;
    void timerCallback() override;
    
    void decayChanged();
    void setDecayRate(float newRate);
    void holdTimeBoxChanged();
    void setHoldTime(int newHoldTime);
    void setAverageDuration(int avergDuration);
    void avrgBoxChanged();
    void gonioScaleChanged();
    void showHoldTicks();
    void setMeterView(MeterViewType vt);
    void meterBoxChanged();
    void setHistoView(HistoViewType vt);
    void histoBoxChanged();
    void resetDecayingTick();
private:
    DataType buffer;
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Pfmcpp_project10AudioProcessor& processor;
    HistogramContainer histograms;
    GonioAndCorrelationWidget gonioAndCorrelationWidget;
    StereoMeter rmsMeter, magnitudeMeter;
    SelectBoxes menus;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Pfmcpp_project10AudioProcessorEditor)
};


