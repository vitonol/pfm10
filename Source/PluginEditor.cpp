/*
 ==============================================================================
 
 This file was auto-generated!
 
 It contains the basic framework code for a JUCE plugin editor.
 
 ==============================================================================
 */

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <iostream>
#include <functional>

//==============================================================================
ColourGradient setGradientPaint(double topColorX, double topColorY, double bottomColorX, double bottomColorY, double zeroDbColorProportion, double middleColorProportion)
{
    ColourGradient cg (Colours::red,
                       topColorX, topColorY,
                       Colours::deepskyblue.darker().darker(),
                       bottomColorX, bottomColorY,
                       false );
    
    cg.addColour(zeroDbColorProportion, Colours::red);
    cg.addColour(zeroDbColorProportion + 0.01, Colours::yellow.darker() );
    cg.addColour(middleColorProportion, Colours::yellowgreen.darker() );
    return cg;
}

//==============================================================================
void TextMeter::paint(juce::Graphics &g)
{
    String str;
    if( vh.isOverThreshold() )
    {
        g.setColour(Colours::red.brighter());
        g.setOpacity(0.8f);
        g.fillRect(getLocalBounds().reduced(0, 1));
        str = String(vh.getCurrentValue(), 1);
    }
    else if (inputDb <= NegativeInfinity)
    {
        str = String("-inf");
    }
    else
    {
        str = String( inputDb, 1 );
    }
    g.setColour(Colours::white);
    g.setFont(10.f);
    g.drawFittedText(str, getLocalBounds(), Justification::centred, 1);
}

void TextMeter::update(float decibels)
{
    inputDb = decibels;
    vh.updateHeldValue(inputDb);
    repaint();
}
//==============================================================================
auto mapY (float dB, float h) { return jmap(dB, NegativeInfinity, MaxDecibels, static_cast<float>(h), 0.f); }

//==============================================================================
Meter::Meter()
{
    showHoldTicks = true;
}
void Meter::paint(juce::Graphics &g)
{
    auto bounds = getLocalBounds();
    auto normalizedDb = jmap( levelDb, NegativeInfinity, MaxDecibels, 0.f, 1.f );
    auto h = bounds.getHeight();
    auto left = 0;
    auto right = getWidth();
    auto top = h * (1.f - normalizedDb );
    auto bottom = 0; // not bottom, but we inverse the layer with opacity
    auto boundsOfFilling = juce::Rectangle<int>::leftTopRightBottom(left, top, right, bottom);
    
    //fills area with color:
    g.setGradientFill(myGrad);
    g.fillRect(bounds);
    
    //fills level:
    g.beginTransparencyLayer(0.89);
    g.setColour(Colours::black);
    g.fillRect(boundsOfFilling);
    g.endTransparencyLayer();
    
//    draws ticks:
    bool toggle = true;
    for (int dB = NegativeInfinity; dB < MaxDecibels; dB += 3)
    {
        auto y = mapY(dB, h);
        auto tickColor = (dB == 0) ? juce::Colours::white.brighter() : juce::Colours::grey.darker();
        g.setColour(tickColor);
        if(getWidth() > 20)
            g.drawHorizontalLine(y, left, right);
        auto gap = toggle ? 0.4 : 0.23;
        left = getWidth() * gap;
        right = getWidth() * (1 - gap);
        toggle = !toggle;
    }
    
    //draws Hold Ticks:
    if (showHoldTicks)
    {
        float y;
        if (!useInfiniteHoldTime)
        {
            y = h * (1.f - jmap( dVH.getCurrentValue(), NegativeInfinity, MaxDecibels, 0.f, 1.f ) );
        }
        else
        {
            y = h * (1.f - jmap( decayingTickValue, NegativeInfinity, MaxDecibels, 0.f, 1.f ) );
        }
        g.setGradientFill(myGrad);
        g.drawLine(0, y, getWidth(), y, 1.5f);
    }
}

void Meter::update(float decibels)
{
    levelDb = decibels;
    if (useInfiniteHoldTime)
    {
//        DBG("infn is true " );
        if (decibels > decayingTickValue)
        {
            DBG("decib is greater than TICK: " << decibels );
            decayingTickValue = decibels;
        }
//        decayingTickValue = jmax(decayingTickValue, decibels);
    }
    else
        dVH.updateHeldValue(levelDb);
    
    repaint();
}

void Meter::resized()
{
    auto h = getLocalBounds().getHeight();
    tickPos.clear();
    for (float dB = NegativeInfinity; dB < MaxDecibels+6;  dB += 6.f)
    {
        Tick t;
        t.dB = dB;
        t.y = mapY(dB, h);
        tickPos.emplace_back(t);
    }
    
    myGrad.clearColours();
    //    auto zeroDb = jmap(levelDb, 0.f, MaxDecibels, 1.f, 0.f); // wrong wrong
    myGrad = setGradientPaint(0, 0, 0, h, .15, .5);
}

//==============================================================================
Scale::Scale(bool scaleCentered) : centeredText(scaleCentered) {}
void Scale::resized() {}
void Scale::paint(juce::Graphics &g)
{
    g.setColour (Colours::grey.brighter().brighter());
    g.setFont (juce::Font (Font::getDefaultMonospacedFontName(), 9.5f, juce::Font::plain));
    
    for (auto tick : tickPos)
    {
        auto text = juce::String(tick.dB);
        auto strWidth = g.getCurrentFont().getStringWidth(text);
        auto space = 0;
        if(centeredText)
            space = (getWidth() - strWidth) /2 ;
        g.drawSingleLineText(text, space, tick.y  + meterOffset);
    }
}
//==============================================================================

MacroMeter::MacroMeter(bool avOnLeft) : averageOnLeft(avOnLeft)
{
    addChildComponent(averageMeter);
    addChildComponent(instantMeter);
    
    addAndMakeVisible(meterLabel);
    addAndMakeVisible(textMeter);
}

void MacroMeter::resized()
{
    auto bounds = getLocalBounds();
    auto meterHight = getHeight() * 0.85;
    auto textMeterX{0}, phatMeterX{0}, tinyMeterX{0};
    auto setTinyMeterX = [bounds] (float coefficient) { return bounds.getX() + bounds.getWidth() * coefficient; };
    if (!averageOnLeft)
    {
        tinyMeterX = setTinyMeterX(.55);
    }
    else
    {
        phatMeterX = getWidth() - (getWidth() / 2);
        textMeterX = phatMeterX;
        tinyMeterX = setTinyMeterX(.35);
    }
    textMeter.setBounds(bounds.withHeight(getHeight() / 13.f)
                        .withWidth(getWidth() / 2)
                        .withX(textMeterX) );
    
    instantMeter.setVisible(meterView == MeterViewType::Both || meterView == MeterViewType::Peak);
    averageMeter.setVisible(meterView == MeterViewType::Both || meterView == MeterViewType::Avg);
    
    //default bounds setting for instatnt meter:
    instantMeter.setBounds(bounds.withWidth(getWidth() / 2)
                           .withX(phatMeterX)
                           .withHeight(meterHight)
                           .withY(textMeter.getHeight() ));

    if (meterView == Both)
    {
        averageMeter.setBounds(instantMeter.getBounds()
                           .withWidth(instantMeter.getWidth() * 0.25)
                           .withX(tinyMeterX));
    }
    else if (meterView == Avg)
    {
        averageMeter.setBounds(instantMeter.getBounds());
    }
    else
    {
        //nothing here, all set by default
    }
}

void MacroMeter::update(float decibels)
{
//    DBG("meter is set to: " << meterView);
    textMeter.update(decibels);
    instantMeter.update(decibels);
    averager.add(decibels);
    averageMeter.update(averager.getAverage());
}

//==============================================================================
StereoMeter::StereoMeter(juce::String meterType) : _meterType(meterType), leftChan(false), rightChan(true), scale(true)
{
    addAndMakeVisible(meterLabel);
    addAndMakeVisible(scale);
    addAndMakeVisible(leftChan);
    addAndMakeVisible(rightChan);
}
void StereoMeter::paint(Graphics& g)
{
    auto font = juce::Font (14.0f, juce::Font::bold);
    auto bounds = getLocalBounds();
    g.fillAll(Colours::black);
    g.setColour(Colours::grey.brighter().brighter());
    g.setFont(font);
    g.drawFittedText("L", bounds.translated(4, 0), Justification::bottomLeft, 1);
    g.drawFittedText("R", bounds.translated(-4, 0), Justification::bottomRight, 1);
    
    meterLabel.setFont(font);
    
}
void StereoMeter::resized()
{
    auto bounds = getLocalBounds();
    auto halfWidth = getWidth() /2;
    meterLabel.setBounds(bounds);
    meterLabel.setText(_meterType, juce::dontSendNotification);
    meterLabel.setColour(Label::textColourId, Colours::grey.brighter().brighter());
    meterLabel.setJustificationType(Justification::centredBottom);
    
    leftChan.setBounds(bounds.withWidth(halfWidth));
    rightChan.setBounds(bounds.withWidth(halfWidth).withX(halfWidth));
    
    scale.tickPos = leftChan.getInstantMeterObject().tickPos;
    scale.meterOffset = leftChan.getInstantMeterObject().getY();
    scale.setBounds(bounds);
}

void StereoMeter::update(float leftChannel, float rightChannel)
{
    leftChan.update(leftChannel);
    rightChan.update(rightChannel);
}
//==============================================================================

Histogram::Histogram(juce::String meterType)
    : _meterType(meterType),
    scale(false)
{
    addAndMakeVisible(scale);
    addAndMakeVisible(histoLabel);
    histoLabel.setInterceptsMouseClicks(false, false); //"band aid" fix
    scale.setInterceptsMouseClicks(false, false);
}

void Histogram::paint(Graphics& g)
{
    g.fillAll(Colours::black);
    auto font = juce::Font (14.0f, juce::Font::bold);
    histoLabel.setFont(font);
    
    auto bounds = getLocalBounds();
    auto h = bounds.getHeight();
    float w = bounds.getWidth();
    auto left = 0;
    auto right = getWidth();
    
    g.setGradientFill(myGrad);
//    g.fillRect(bounds);
    
    //draws path:
    auto& data = histoBuffer.getData();
    path.clear();
    auto readIndex = histoBuffer.getReadIndex();
    int counter = 0;
    int x;
    Point<float> point;
    /* when x < w it is not going all the way to the right edge
     when x <= it does
     bc x is <= w is 1 greater than width, we need to add 1 to the buffer size, which is what we do in the resize, to account for the missing pixel
     */
    for (x = 0; x <= w; x++)
    {
        counter ++;
        auto val = data[readIndex];
        auto mappedY = mapY(val, h);
        point.x = x;
        point.y = mappedY;
        if (x == w - 1)
        {
            DBG("value: " << val);
        }
        
        if( x == 0 )
            path.startNewSubPath(point);
        else
            path.lineTo(point);
        readIndex++;
        if (readIndex > data.size() - 1)
            readIndex = 0;
    }
    
//    path.applyTransform(AffineTransform::scale((float)w / (float)data.size(), 1));
    path.lineTo(bounds.getBottomRight().toFloat());
    path.lineTo(bounds.getBottomLeft().toFloat());

    path.closeSubPath();

    g.fillPath(path);
//    g.endTransparencyLayer();
    
    //draws scale lines
    for (int dB = NegativeInfinity-6; dB < MaxDecibels; dB += 18)
    {
        auto y = mapY(dB, h);
        g.setColour(Colours::darkgrey);
        g.drawHorizontalLine(y, left, right);
    }
//    g.setColour(Colours::white);
//    g.drawRect(bounds);
}


void Histogram::resized()
{
    auto bounds = getLocalBounds();
    auto w = bounds.getWidth();
    auto h = bounds.getHeight();
    
    histoLabel.setBounds(bounds);
    histoLabel.setText(_meterType, juce::dontSendNotification);
    histoLabel.setColour(Label::textColourId, Colours::grey.brighter().brighter());
    histoLabel.setJustificationType(Justification::centredTop);
    
    histoBuffer.resize(w + 1, NegativeInfinity); //added + 1 to cover up a dip in the right side of the path
    path.preallocateSpace(w * 3);
    Tick t;
    tickPos.clear();
    for (float dB = NegativeInfinity-6; dB < MaxDecibels;  dB += 18.f)
    {
        t.dB = dB;
        t.y = mapY(dB, h);
        tickPos.emplace_back(t);
    }
    
    scale.tickPos = tickPos;
    scale.meterOffset = bounds.getY() + (bounds.getHeight() * 0.02);
    scale.setBounds(bounds);
    
    myGrad.clearColours();
    myGrad = setGradientPaint(0, 0, 0, h, .15, .5);
}

void Histogram::update(float decibels)
{
    histoBuffer.write(decibels);
    repaint();
}

//==============================================================================

HistogramContainer::HistogramContainer() : rmsHisto("RMS"), peakHisto("PEAK")
{
    addAndMakeVisible(rmsHisto);
    addAndMakeVisible(peakHisto);
}

void HistogramContainer::resized()
{
    auto bounds = getLocalBounds();
    juce::FlexBox fb;
    
    fb.flexDirection = (histoView == HistoViewType::SideBySide ? FlexBox::Direction::row : FlexBox::Direction::column);
    fb.items.add(FlexItem(rmsHisto).withFlex(1.0));
    fb.items.add(FlexItem().withFlex(0.02));
    fb.items.add(FlexItem(peakHisto).withFlex(1.0));
    
    fb.performLayout(bounds.toFloat());
    
}

//==============================================================================
Goniometer::Goniometer(juce::AudioBuffer<float> & _myBuffer) : myBuffer(_myBuffer)
{
    scaleSize = 0.7;
}

Point<float> Goniometer::getXY(float left, float right, Point<float> center, float radius)
{
    auto scale = static_cast<float>(scaleSize);  // controllable by comboBox
    auto side = ((left-right) * subtract3dB) * scale;
    auto mid = ((left + right) * subtract3dB) * scale;
    return {center.getX() + radius * side, center.getY() + radius * mid};
}


void Goniometer::paint(juce::Graphics &g)
{
    g.fillAll(Colours::black);

    auto bounds = getLocalBounds().reduced(12.f).toFloat();
    g.drawImage(gonioGrid, bounds, RectanglePlacement::doNotResize);
    
    auto bufferSize = myBuffer.getNumSamples(); // same thing here, does not want to take passed value from editor
    if (bufferSize == 0)
        return; // execution of paint function stops here if condition is true
    
    auto center = bounds.getCentre();
    auto radius = bounds.getWidth() /2 ;
    int increment = 1;
    if (bufferSize >= 512)
    {
        increment = 2;
    }
    else if (bufferSize >= 1024)
    {
        increment = 3;
    }
    path.clear();
    path.startNewSubPath(getXY(myBuffer.getSample(0, 0), myBuffer.getSample(1, 0), center, radius));
    for( int i = 1; i < bufferSize; i += increment )
    {
        path.lineTo(getXY(myBuffer.getSample(0, i), myBuffer.getSample(1, i), center, radius));
    }
    
    g.setGradientFill(myGrad);
    g.strokePath(path, PathStrokeType (0.85f) );
}

void Goniometer::generateImage()
{
    Graphics g{ gonioGrid };
    //grid drawings:
    auto gap = 15;
    auto bounds = getLocalBounds().toFloat();
    bounds.reduce( (float)gap, (float)gap );
    auto center = bounds.getCentre();
    auto radius = bounds.getWidth() /2;
    
    Path p;
    for (int i = 0; i < 8; i++)
    {
        p.startNewSubPath(center);
        p.lineTo(center.getPointOnCircumference(radius, degreesToRadians(i * 45.f) ));
    }
    
    g.setColour(Colours::grey);
    g.strokePath(p, PathStrokeType (0.5f) );
    g.drawEllipse(bounds, 1.f);
    
    g.setColour(Colours::white);
    g.setFont(gap);
    Array<String> chars{"+S", "L", "M", "R", "-S"};
    for(int i = 0; i < chars.size(); i++)
    {
        auto width = Font(gap).getStringWidth(chars.getReference(i));
        Rectangle<float> r(width, gap);
        r.setCentre(center.getPointOnCircumference(radius + 8, (degreesToRadians(45.f)) * (6 + i) ) );
        g.drawFittedText(chars.getReference(i), r.toNearestInt(),
                         Justification::centred, 1);
    }
}

void Goniometer::resized()
{
    auto bounds = getLocalBounds().toFloat();
    auto center = bounds.getCentre();
    auto radius = bounds.reduced(20).getWidth() / 2;
    myGrad.clearColours();
    myGrad = ColourGradient(Colours::deepskyblue.darker().darker(),
                           center.getX(), center.getY(),
                           Colours::yellowgreen.darker(),
                           center.getPointOnCircumference(radius, degreesToRadians(90.f)).getX(),
                           center.getPointOnCircumference(radius, degreesToRadians(90.f)).getY(),
                           true);
    
    path.preallocateSpace(bounds.getWidth() * 3);
    gonioGrid = Image(Image::PixelFormat::RGB, bounds.getWidth(), bounds.getHeight(), true);
    
    generateImage();
}

void Goniometer::update()
{
    repaint();
}

//==============================================================================

void CorrelationMeter::paint(juce::Graphics &g)
{
    auto bounds = getLocalBounds();
    auto center = bounds.getCentreX();
    auto w = bounds.getWidth();
    auto x = center + (w / 2) * correlationValue;
    auto top = bounds.getY();
    auto bottom = bounds.getBottom();
    auto boundsOfFilling = juce::Rectangle<int>::leftTopRightBottom(center, top, x, bottom);
   
    g.setGradientFill( x >= center ? positiveGrad : negativeGrad);
    if (correlationValue != 0)
        g.fillRect(boundsOfFilling);
}

void CorrelationMeter::resized()
{
    auto bounds = getLocalBounds();
    positiveGrad.clearColours();
    negativeGrad.clearColours();
    
    positiveGrad = setGradientPaint(bounds.getCentreX(), 0,
                                    bounds.getRight(), 0,
                                    0, 0);
    negativeGrad = setGradientPaint(bounds.getCentreX(), 0,
                                    bounds.getX(), 0,
                                    0.99, 0);
}

void CorrelationMeter::update(float correlation)
{
    correlationValue = correlation;
    repaint();
}

//==============================================================================
CorrelationComputation::CorrelationComputation(juce::AudioBuffer<float> & _myBuffer, double sampleRate) : myBuffer(_myBuffer)
{
    auto coef = IIR::Coefficients<float>::makeLowPass(sampleRate, 100.f);
    for(auto& filter : filters)
        filter = IIR::Filter<float>(coef); // initializes coefficients
    addAndMakeVisible(correlationMeter);
    addAndMakeVisible(avrgdCorrelationMeter);
    
}

void CorrelationComputation::paint(juce::Graphics &g)
{
    auto bounds = getLocalBounds();
    g.setColour(Colours::grey);
    g.drawRect(bounds);
    g.drawVerticalLine(bounds.getCentreX(), 0, bounds.getBottom());
}

float CorrelationComputation::computeCorrelation()
{
    float correlation{0};
    for( int i = 0; i < myBuffer.getNumSamples() / 2; i += 2 )
    {
        auto left = myBuffer.getSample(0, i);
        auto right = myBuffer.getSample(1, i);
        auto numerator = filters[0].processSample(left * right);
        auto denominator = sqrt(filters[1].processSample(left * left) * filters[2].processSample(right * right));
        if(denominator != 0)
        {
            correlation = numerator / denominator;
            averager.add(correlation);
            instantCorr.add(correlation);
        }
        else
        {
            averager.add(0.f);
            instantCorr.add(0.f);
        }
    }
    return 0;
    
}
void CorrelationComputation::resized()
{
    auto bounds = getLocalBounds();
    auto h = bounds.getHeight();
    avrgdCorrelationMeter.setBounds(bounds.withY(1).withHeight(h * 0.25f));
    correlationMeter.setBounds(bounds.withY(6).withHeight(h * .48f));
}

void CorrelationComputation::update()
{
    if (myBuffer.getNumSamples() != 0)
        computeCorrelation();
    correlationMeter.update(instantCorr.getAverage() );
    avrgdCorrelationMeter.update(averager.getAverage() );
    repaint();
}
//==============================================================================
GonioAndCorrelationWidget::GonioAndCorrelationWidget(juce::AudioBuffer<float> & myBuffer, double sampleRate) : goniometer(myBuffer), correlationMeter(myBuffer, sampleRate)
{
    addAndMakeVisible(goniometer);
    addAndMakeVisible(correlationMeter);
}

void GonioAndCorrelationWidget::paint(juce::Graphics &g)
{
    g.fillAll(Colours::black);
    
    auto correlationBounds = correlationMeter.getBounds().expanded(20, 0);
    g.setColour(Colours::white);
    g.setFont(12.f);
    g.drawFittedText("-1", correlationBounds, Justification::left, 1);
    g.drawFittedText("+1", correlationBounds, Justification::right, 1);
}

void GonioAndCorrelationWidget::resized()
{
    auto bounds = getLocalBounds();
    auto h = bounds.getHeight();
    goniometer.setBounds(bounds.withSizeKeepingCentre(h, h) );
    correlationMeter.setBounds(goniometer.getBounds().withHeight(h / 16).withY(bounds.getBottom() - (h / 16)) );
}

void GonioAndCorrelationWidget::update(const juce::AudioBuffer<float> &buffer)
{
    goniometer.update();
    correlationMeter.update();
    repaint();
}
//==============================================================================
SelectBoxes::SelectBoxes()
{
    dropDownLists[BoxIndex::DecayRate] = StringArray({"-3dB/s", "-6dB/s", "-12dB/s", "-24dB/s", "-48dB/s" });
    dropDownLists[BoxIndex::AverageAmount] = StringArray({"100ms", "250ms", "500ms", "1000ms", "2000ms" });
    dropDownLists[BoxIndex::MeterView] = StringArray({"Both", "Peak", "Avg" });
    dropDownLists[BoxIndex::HoldTime] = StringArray({"0s", "0.5s", "1s", "2s", "4s", "inf" });
    dropDownLists[BoxIndex::HistoView] = StringArray({"Stacked", "Side-by-Side" });
    
//    boxes[0].addSectionHeading("Select Decay Rate: "); // makes it look too busy...
//    boxes[1].addSectionHeading("Select Average Duration: ");
//    boxes[2].addSectionHeading("Select Meter View: ");
//    boxes[3].addSectionHeading("Select Hold Time: ");
//    boxes[4].addSectionHeading("Select Histogram View: ");
    
    labels[BoxIndex::DecayRate].setText("DECAY RATE", dontSendNotification);
    labels[BoxIndex::AverageAmount].setText("AVERAGE DURATION", dontSendNotification);
    labels[BoxIndex::MeterView].setText("METER VIEW", dontSendNotification);
//    labels[BoxIndex::HoldTime].setText("HOLD TIME", dontSendNotification);
    labels[BoxIndex::HistoView].setText("HISTO VIEW", dontSendNotification);
    labels[BoxIndex::ScaleSlider].setText("SCALE", dontSendNotification);
    
    for (auto& l : labels)
    {
        addAndMakeVisible(l);
        l.setFont(textFont);
    }
    
    for (auto& b : boxes)
    {
        addAndMakeVisible(b);
        b.setLookAndFeel(&boxLandF);
    }
    
    for (int i = 0; i < 5; i++)
    {
        boxes[i].addItemList(dropDownLists[i], 1); // hmm can this move to loop that iterates thru boxes?
    }

    addAndMakeVisible(scaleSlider);
    scaleSlider.setRange(0.5, 2.0);
    scaleSlider.setValue(1.0);
    
    addAndMakeVisible(holdButton);
    holdButton.setToggleState(true, dontSendNotification); // on by default
    holdButton.setClickingTogglesState(true);
    
    addChildComponent(resetHoldButton);

}

void SelectBoxes::resized()
{
    auto bounds = getBounds();
    //it hurts to look at code below. gotta find a better way ...
    labels[BoxIndex::DecayRate].setBounds(10, 0, 71, 30);
    boxes[BoxIndex::DecayRate].setBounds(labels[BoxIndex::DecayRate].getBounds().
                                         withY(labels[BoxIndex::DecayRate].getHeight()));
    
    labels[BoxIndex::AverageAmount].setBounds(boxes[BoxIndex::DecayRate].getBounds().
                                              withY(bounds.getHeight() / 3 ));
    boxes[BoxIndex::AverageAmount].setBounds(labels[BoxIndex::AverageAmount].getBounds().
                                             withY(labels[BoxIndex::AverageAmount].getY() + labels[BoxIndex::AverageAmount].getHeight()));
    
    labels[BoxIndex::MeterView].setBounds(boxes[BoxIndex::AverageAmount].getBounds().
                                          withY(bounds.getHeight() * 0.65));
    boxes[BoxIndex::MeterView].setBounds(labels[BoxIndex::MeterView].getBounds().
                                         withY(labels[BoxIndex::MeterView].getY() + labels[BoxIndex::MeterView].getHeight()));
    
    labels[BoxIndex::HoldTime].setBounds(labels[BoxIndex::DecayRate]
                                         .getLocalBounds().withX(bounds.getWidth() - bounds.getWidth() * 0.15f)
                                         .withY(bounds.getHeight() / 3 ));
    
    holdButton.setBounds(labels[BoxIndex::HoldTime].getBounds());
    boxes[BoxIndex::HoldTime].setBounds(labels[BoxIndex::HoldTime].getBounds().
                                        withY(labels[BoxIndex::HoldTime].getY() + labels[BoxIndex::HoldTime].getHeight()));
    
    resetHoldButton.setVisible(true);
    resetHoldButton.setBounds(boxes[BoxIndex::HoldTime].getBounds()
                        .withY(boxes[BoxIndex::HoldTime].getY() + boxes[BoxIndex::HoldTime].getHeight())
                        .reduced(1, 8));
    
    
    labels[BoxIndex::HistoView].setBounds(boxes[BoxIndex::HoldTime].getBounds().
                                          withY(bounds.getHeight() * 0.65));
    boxes[BoxIndex::HistoView].setBounds(labels[BoxIndex::HistoView].getBounds().
                                         withY(labels[BoxIndex::HistoView].getY() + labels[BoxIndex::HistoView].getHeight()));
    
    labels[BoxIndex::ScaleSlider].setBounds(labels[BoxIndex::DecayRate].getBounds().
                                            withX(labels[BoxIndex::HoldTime].getX()));
    scaleSlider.setBounds(labels[BoxIndex::ScaleSlider].getBounds().
                          withY(boxes[BoxIndex::DecayRate].getY()).expanded(15));
    
}

void SelectBoxes::paint(juce::Graphics &g)
{
    g.fillAll(Colours::transparentBlack);
}

void BoxLookAndFeel::getIdealPopupMenuItemSize    (    const String &     text,
                              bool     isSeparator,
                              int     standardMenuItemHeight,
                              int &     idealWidth,
                              int &     idealHeight
                              )
{
    auto t = text.length();
    idealWidth = t;
}


void BoxLookAndFeel::drawPopupMenuItem    (    Graphics & g ,
                           const Rectangle< int > &     area,
                           bool     isSeparator,
                           bool     isActive,
                           bool     isHighlighted,
                           bool     isTicked,
                           bool     hasSubMenu,
                           const String &     text,
                           const String &     shortcutKeyText,
                           const Drawable *     icon,
                           const Colour *     textColour
                           )
{
    g.fillAll(Colours::black);
    g.setColour(Colours::gold);
    g.drawRect(area.reduced(1));
    g.setColour(Colours::whitesmoke);
    g.drawText(text, area, Justification::centred);
}

//==============================================================================



Pfmcpp_project10AudioProcessorEditor::Pfmcpp_project10AudioProcessorEditor (Pfmcpp_project10AudioProcessor& p)
: AudioProcessorEditor (&p), processor (p),
rmsMeter("RMS"), magnitudeMeter("PEAK"),
gonioAndCorrelationWidget(buffer, processor.getSampleRate() )

{
//    buffer.setSize(buffer.getNumChannels(), buffer.getNumSamples() ); // do I need this ? and what to pass, because I think I am setting args to iteslf here...
    buffer.clear(); // buffer does not have channels, when
    addAndMakeVisible(rmsMeter);
    addAndMakeVisible(magnitudeMeter);
    addAndMakeVisible(histograms);
    
    addAndMakeVisible(gonioAndCorrelationWidget);
    addAndMakeVisible(menus);
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    
    setSize (720, 480);
    startTimerHz(TimerFreqHz);
    
    menus.boxes[BoxIndex::DecayRate].onChange = [this] { decayChanged(); };
    menus.boxes[BoxIndex::DecayRate].setSelectedId(4);
    
    menus.boxes[BoxIndex::HoldTime].onChange = [this] { holdTimeBoxChanged(); };
    menus.boxes[BoxIndex::HoldTime].setSelectedId(2);
    
    menus.boxes[BoxIndex::AverageAmount].onChange = [this] { avrgBoxChanged(); };
    menus.boxes[BoxIndex::AverageAmount].setSelectedId(4);
    
    menus.boxes[BoxIndex::MeterView].onChange = [this] { meterBoxChanged(); };
    menus.boxes[BoxIndex::MeterView].setSelectedId(1);
    
    menus.boxes[BoxIndex::HistoView].onChange = [this] { histoBoxChanged(); };
    menus.boxes[BoxIndex::HistoView].setSelectedId(2);
    
    menus.scaleSlider.onValueChange = [this] { gonioScaleChanged(); };
    
    menus.holdButton.onClick = [this] { showHoldTicks(); };
    
    menus.resetHoldButton.onClick = [this] { resetDecayingTick(); };
}

void Pfmcpp_project10AudioProcessorEditor::resetDecayingTick()
{
    rmsMeter.resetDecayingTick();
    magnitudeMeter.resetDecayingTick();
}

void Pfmcpp_project10AudioProcessorEditor::setHistoView(HistoViewType vt)
{
    histograms.setHistoView(vt);
}

void Pfmcpp_project10AudioProcessorEditor::histoBoxChanged()
{
    switch (menus.boxes[BoxIndex::HistoView].getSelectedId())
    {
        case 1: setHistoView(HistoViewType::Stacked); break;
        case 2: setHistoView(HistoViewType::SideBySide); break;
        default:
            break;
    }
}

void Pfmcpp_project10AudioProcessorEditor::setMeterView(MeterViewType vt)
{
    rmsMeter.setMeterView(vt);
    magnitudeMeter.setMeterView(vt);
}
void Pfmcpp_project10AudioProcessorEditor::meterBoxChanged()
{
    switch (menus.boxes[BoxIndex::MeterView].getSelectedId())
    {
        case 1: setMeterView(MeterViewType::Both); break;
        case 2: setMeterView(MeterViewType::Peak); break;
        case 3: setMeterView(MeterViewType::Avg); break;
        default:
            break;
    }
}

void Pfmcpp_project10AudioProcessorEditor::showHoldTicks()
{
    auto on = menus.holdButton.getToggleState();
    rmsMeter.showHoldTicks(on);
    magnitudeMeter.showHoldTicks(on);
    menus.boxes[BoxIndex::HoldTime].setVisible(!on ? false : true );
}

void Pfmcpp_project10AudioProcessorEditor::gonioScaleChanged()
{
    gonioAndCorrelationWidget.updateScaleSize(menus.scaleSlider.getValue() );
}

void Pfmcpp_project10AudioProcessorEditor::setAverageDuration(int avergDuration)
{
    rmsMeter.setAveragerSize(avergDuration);
    magnitudeMeter.setAveragerSize(avergDuration);
    DBG("average duration is set to: " << avergDuration);
}

void Pfmcpp_project10AudioProcessorEditor::avrgBoxChanged()
{
    switch (menus.boxes[BoxIndex::AverageAmount].getSelectedId())
    {
        case 1: setAverageDuration(100);  break;
        case 2: setAverageDuration(250);  break;
        case 3: setAverageDuration(500);  break;
        case 4: setAverageDuration(1000); break;
        case 5: setAverageDuration(2000); break;
        default:
            break;
    }
}
    
 void Pfmcpp_project10AudioProcessorEditor::setDecayRate(float newRate)
 {
     rmsMeter.setDecayRate(newRate);
     magnitudeMeter.setDecayRate(newRate);
     DBG("decay rate is: -" << newRate << "db/sec");
 }

 void Pfmcpp_project10AudioProcessorEditor::decayChanged()
 {
     switch (menus.boxes[BoxIndex::DecayRate].getSelectedId() )
     {
         case 1: setDecayRate(3.f); break;
         case 2: setDecayRate(6.f); break;
         case 3: setDecayRate(12.f); break;
         case 4: setDecayRate(24.f); break;
         case 5: setDecayRate(48.f); break;
         default:
             break;
     }
 }


 void Pfmcpp_project10AudioProcessorEditor::setHoldTime(int newHoldTime)
 {
     rmsMeter.setHoldTime(newHoldTime);
     magnitudeMeter.setHoldTime(newHoldTime);
 }

 void Pfmcpp_project10AudioProcessorEditor::holdTimeBoxChanged()
 {
     rmsMeter.setInfiniteHold(false);
     magnitudeMeter.setInfiniteHold(false);
     auto selectedID = menus.boxes[BoxIndex::HoldTime].getSelectedId();
     bool isHoldInfinite = selectedID == 6;
     menus.resetHoldButton.setVisible(isHoldInfinite);
     switch (selectedID)
     {
         case 1: setHoldTime(0); break;
         case 2: setHoldTime(500); break;
         case 3: setHoldTime(1000); break;
         case 4: setHoldTime(2000); break;
         case 5: setHoldTime(4000); break;
         case 6:
         {
             rmsMeter.setInfiniteHold(true);
             magnitudeMeter.setInfiniteHold(true);
             break;
         }
         default:
             break;
     }
 }
 

Pfmcpp_project10AudioProcessorEditor::~Pfmcpp_project10AudioProcessorEditor()
{
}

void Pfmcpp_project10AudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
    
    g.setColour (Colours::lightcyan);
    g.setFont (18.0f);
    g.drawFittedText ("Audio Metering Plugin", getLocalBounds(), Justification::centredTop, 1);
    
}


void Pfmcpp_project10AudioProcessorEditor::timerCallback()
{
    if( processor.fifo.pull(buffer) )
    {
        auto leftRms = Decibels::gainToDecibels( (buffer.getRMSLevel(0, 0, buffer.getNumSamples()) ), NegativeInfinity);
        auto rightRms = Decibels::gainToDecibels( (buffer.getRMSLevel(1, 0, buffer.getNumSamples()) ), NegativeInfinity);
        auto leftPeak = Decibels::gainToDecibels( (buffer.getMagnitude(0, 0, buffer.getNumSamples()) ), NegativeInfinity);
        auto rightPeak = Decibels::gainToDecibels( (buffer.getMagnitude(1, 0, buffer.getNumSamples()) ), NegativeInfinity);
//        DBG("peak: " << leftPeak);
        rmsMeter.update(leftRms, rightRms);
        magnitudeMeter.update(leftPeak, rightPeak);
        
        histograms.rmsHisto.update((leftRms + rightRms) / 2);
        histograms.peakHisto.update((leftPeak + rightPeak) / 2);
        
        gonioAndCorrelationWidget.update(buffer);

    }
}


void Pfmcpp_project10AudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    
    auto editorBounds = Pfmcpp_project10AudioProcessorEditor::getBounds();
    
    
    rmsMeter.setBounds( editorBounds.getX()+(editorBounds.getWidth() / 120),
                       editorBounds.getY()+(editorBounds.getHeight() / 20),
                       editorBounds.getWidth() * 0.12f,
                       editorBounds.getHeight() * 0.5f );
    magnitudeMeter.setBounds(rmsMeter.getBounds().withX(getWidth() * 0.874f ));
    
    auto meterBounds = rmsMeter.getBounds();
    
    menus.setBounds(editorBounds
                    .withSizeKeepingCentre(editorBounds.getWidth() * 0.73f, meterBounds.getHeight())
                    .withY(meterBounds.getY()) );
    
    histograms.setBounds(editorBounds
                         .withSizeKeepingCentre(editorBounds.getWidth() * .98f,
                                                editorBounds.getHeight() * .42f)
                                                 .withY(meterBounds.getHeight() * 1.15f));
    
    
    gonioAndCorrelationWidget.setBounds(editorBounds
                                        .withSizeKeepingCentre(editorBounds.getWidth() * 0.45f, meterBounds.getHeight())
                                        .withY(meterBounds.getY()) );
    
}


