/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
 REFERENCE:
 theaudioprogrammer.com/about-lifo-and-fifo-data-structures-the-basics/
*/ 
#include <array>
#define OscillatorIsON 0

template <typename T>
struct Fifo //passes data from the AudioThread to the GUI thread.
{
    void prepare (int numSamples, int numChannels) // call this in prepareToPlay to initialize buffers.
    {
        for (auto& buffer : containerFifoBuffer)
        {
            /*
             void setSize (int newNumChannels,
             int newNumSamples,
             bool keepExistingContent = false,
             bool clearExtraSpace = false,
             bool avoidReallocating = false)
             */
            buffer.setSize(numChannels, numSamples); // channels is 1st variable
            buffer.clear();
        }
    }
    
    bool push (const T& itemToAdd)
    {
        auto write = fifo.write(1);
        if (write.blockSize1 >= 1)
        {
            containerFifoBuffer[write.startIndex1] = itemToAdd;
            return true;
        }
        return false;
    }
    
    bool pull (T& itemToUpdate)
    {
        auto read = fifo.read(1);
        if( read.blockSize1 >= 1)
        {
            itemToUpdate = containerFifoBuffer[read.startIndex1];
            return true;
        }
        return false;
    }
private:
    static constexpr int Capacity = 3;
    std::array<T, Capacity> containerFifoBuffer;
    AbstractFifo fifo{Capacity};
};
//==============================================================================

//==============================================================================
class Pfmcpp_project10AudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    Pfmcpp_project10AudioProcessor();   
    ~Pfmcpp_project10AudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int maximumExpectedSamplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    //==============================================================================
    Fifo<AudioBuffer<float>> fifo;
#if OscillatorIsON
    juce::dsp::Oscillator<float> wave;
#endif
  private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Pfmcpp_project10AudioProcessor)
};


